#!/usr/bin/php
<?php
#JSN:xhorak61
require_once('fileManager.php');
require_once('settings.php');
require_once('JSON_parser.php');

Settings::checkOptions($argv);
Settings::loadOptions();

// Check input/output files and load data
if(MyFiles::$input_file == null) {
	while(($line = rtrim(fgets(STDIN))) != false) {
		MyFiles::$contents .= $line;
	}
}
else {	// Load input from file
	MyFiles::getContents();
}

// Start parse input file
Parser::parserr();

if(MyFiles::$output_file == null) {
	echo XML_generator::getXMLcontents();
}
else {	// Load input from file
	MyFiles::putFileContents();
}
exit(0);
?>