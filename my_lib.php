<?php

#JSN:xhorak61
const object_first = 0;
const object_second = 1;
const array_val = 2;

//Errors
	const err_unknown_char = 4;
	const err_number = 4;
	const err_keywords = 4;
	const err_string = 4;
	const err_syn = 4;

	const my_EOF = -1;	// End of file

// States
	//Scanner states
		// Scanner
		const st_start = 0;

		// String
		const st_str = 5;		// '"'
		const st_str_rev = 6;	// '\'

		// Number
		const st_minus = 7;
		const st_zero_num = 8;
		const st_num = 9;
		const st_dig = 10;
		const st_num_e = 11;

	// JSON_object states
	const st_object_name = 1;
	const st_object_colon = 2;
	const st_object_end = 3;
	const st_object_value = 4;

	// JSON_array states
	const st_array_val = 1;
	const st_array_end = 2;
	
	// JSON_object + JSON_array
	const st_keywords = 99;

// JSON Tokens
	const t_object = 1;		// {
	const t_end_object = 2;	// }
	const t_Ostring = 3;	// name of object

	const t_array = 4;		// [
	const t_end_array = 5;	// ]

	const t_string = 6;		// string
	const t_number = 7;		// number
	const t_true = 8;		// true
	const t_false = 9;		// false
	const t_null = 10;		// null

	const t_colon = 11;		// :
	const t_comma = 12;		// ,
?>