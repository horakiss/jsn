<?php

#JSN:xhorak61
class Settings
{
        public static $h, $r, $array_name, $item_name, $start = null;
        public static $n, $s, $i, $l, $c, $array_size, $index_items, $mIntput, $mOutput = false;

        public static function setVariable($options)
        {
                foreach (array_keys($options) as $opt)
                {
                        switch ($opt)
                        {
                                case 'help':
                                        self::help();
                                        exit(0);
                                        break;
                                case 'input':
                                        MyFiles::load_input_file($options["input"]);
                                        break;
                                case 'output': 
                                        MyFiles::load_output_file($options["output"]);
                                        break;
                                case 'h':
                                        self::$h = $options["h"];
                                        break;
                                case 'n':
                                        self::$n = true;
                                        break;
                                case 'r':
                                        self::$r = $options["r"];
                                        XML_generator::checkElement(self::$r);
                                        break;
                                case 'array-name':
                                        self::$array_name = $options["array-name"];
                                        XML_generator::checkElement(self::$array_name);
                                        break;
                                case 'item-name':
                                        self::$item_name = $options["item-name"];
                                        XML_generator::checkElement(self::$item_name);
                                        break;
                                case 's':
                                        self::$s = true;
                                        break;
                                case 'i':
                                        self::$i = true;
                                        break;
                                case 'l':
                                        self::$l = true;
                                        break;
                                case 'c':
                                        self::$c = true;
                                        break;
                                case 'a':
                                        if(self::$array_size == true) {
                                                exit(1);
                                        }
                                        self::$array_size = true;
                                        break;
                                case 'array-size':
                                        if(self::$array_size == true) {
                                                exit(1);
                                        }
                                        self::$array_size = true;
                                        break;
                                case 't':
                                        if(self::$index_items == true) {
                                                exit(1);
                                        }
                                        self::$index_items = true;
                                        break;
                                case 'index-items':
                                        if(self::$index_items == true) {
                                                exit(1);
                                        }
                                        self::$index_items = true;
                                        break;
                                case 'start':
                                        self::$start = $options["start"];
                                        if(!is_numeric(self::$start)) {
                                                exit(1);
                                        }
                                        if(self::$start < 0) {
                                                exit(1);
                                        }
                                        self::$start--;
                                        break;
                        }
                }
        }

        public static function loadOptions() {
                $shortopts = "h::nr::silcat";
                $longopts = array(
                        "help",
                        "input:",
                        "output:",
                        "array-name:",
                        "item-name:",
                        "array-size",
                        "index-items",
                        "start::",
                );

                // Load OPTIONS
                $options = getopt($shortopts,$longopts);

                // Parse and set OPTIONS
                Settings::setVariable($options);
                Settings::checkSettings();
        }

        public static function checkOptions($argv) {
                // help
                $param[0] = "/^(--help)$/";
                // files
                $param[1] = "/^--input=(.*)$/";
                $param[2] = "/^--output=(.*)$/";
                // substitution
                $param[3] = "/^-h=(.*)$/";
                // header
                $param[4] = "/^(-n)$/";
                // root-elemnt
                $param[5] = "/^-r=(.*)$/";
                // change array || item name
                $param[6] = "/^--array-name=(.*)$/";
                $param[7] = "/^--item-name=(.*)$/";
                // print value of type string || number || literal as element
                $param[8] = "/^(-s)$/";
                $param[9] = "/^(-i)$/";
                $param[10] = "/^(-l)$/";
                // convert problematic XML characters
                $param[11] = "/^(-c)$/";
                // write array size || index items 
                $param[12] = "/^(-a)$/";
                $param[13] = "/^(-t)$/";
                $param[14] = "/^(--array-size)$/";
                $param[15] = "/^(--index-items)$/";
                // define start of index items
                $param[16] = "/^--start=(.*)$/";
                $param[17] = "/^(jsn\.php)$/";

                $para_count = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

                foreach ($argv as $test) {
                        $isIN = false;
                        if(preg_match($param[0], $test, $result)) {     // Finded --help
                                if(count($argv) > 3) { // Help has to be used alone (jsn.php OPEN.. --help)
                                        exit(1);
                                }
                                break; // Take next $argv
                        }

                        for($i = 1; $i < count($param); $i++) { // Every pattern of options
                                if(preg_match($param[$i], $test, $result)) {    // Compare pattern with option
                                        $para_count[$i]++; // Option was used
                                        $isIN = true; // Option match pattern
                                        break;
                                }
                        }

                        if(!$isIN) {    // Option not match, call error
                                exit(1);
                        }
                        else {
                                $isIN = false;
                        }
                }

                for($i = 0; $i < count($para_count); $i++) {    // Control that options was used once
                        if($para_count[$i] > 1) {
                                exit(1);
                        }
                }
        }

        public static function checkSettings()
        {
                if(self::$start != null && self::$index_items == false) {
                        //echo "Error: You have to combine option --start with --t or --index-items!\n";
                        exit(1);
                }
                if(self::$start == null) {
                        
                        self::$start = 0;
                }
                if(self::$h == null) {
                        self::$h = '-';
                }
                if(self::$array_name == null) {
                        self::$array_name = 'array';
                }
                if(self::$item_name == null) {
                        self::$item_name = 'item';
                }
        }

        public function help()
        {
                echo "Nápověda k programu JSON2XML\nVytvořil Jakub Horák - xhorak61@stud.fit.vutbr.cz\nVytvořeno 18.2.2016\n\n";
                echo "OPTIONS\n
                --help\t\t\tThis help\n
                --input=filename\tinput JSON file in UTF-8\n
                --output=filename\toutput XML file in UTF-8\n";

        }
}
?>