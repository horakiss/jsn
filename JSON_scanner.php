<?php

#JSN:xhorak61
require_once('my_lib.php');
/*
	Class scanner load data from input and send tokens parser
*/
class Scanner {
	private static $state = 0;	// State of scanner
	private static $tmp_string = ""; // Temporary string for save names
	private static $string_uniq = "";

	// Values for number automata
	private static $num_point = true;	// Point was used
	private static $num_e = true;	// e || E was used
	
	/*
		Function returns tokens from input file
		return int - token
	*/
	public static function scannerr() {

		Scanner::$tmp_string = "";	// String for save name
		Scanner::$state = 0;	// Actual state
		Scanner::$num_point = true;	// Watch point in number
		Scanner::$num_e = true; // Watch e || E in number
		Scanner::$string_uniq = ""; // String for unique sequence

		while(($c = MyFiles::rdChar()) != -1) {
			if(Scanner::$state == st_start) {	// Read object || array
				if(Scanner::isWhitespace($c)){
					Scanner::$state = st_start;;
				}
				else if($c == '{') {
					return t_object;
				}
				else if($c == '[') {
					return t_array;
				}
				else if($c == ']') {
					return t_end_array;
				}
				else if($c == ':') {
					return t_colon;
				}
				else if($c == ',') {
					return t_comma;
				}
				else if($c == '}') {
					return t_end_object;	// End Object
				}
				else if($c == '"') {
					Scanner::$state = st_str;	// Start reading string
				}
				else if($c == '0') {
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_zero_num;	// Start reading string
				}
				else if($c == '-') {
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_minus;	// Start reading string
				}
				else if($c >= '1' && $c <= '9') {
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_dig;	// Start reading string
				}
				else if($c >= 'a' && $c <= 'z') {
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_keywords;
				}
				else {
					MyFiles::callError(err_unknown_char, "Lex Error: Unknown character.\n");
				}
			}
			else if(Scanner::$state == st_str) {	// In object
				if($c == '\\') {
					Scanner::$state = st_str_rev;
				}
				else if($c == '"') {
					Scanner::$state = st_start;
					return t_string;
				}
				else {
					Scanner::$tmp_string .= $c;
				}
			}
			else if(Scanner::$state == st_str_rev) {
				Scanner::JSON_string($c);
			}
			else if(Scanner::$state == st_minus) {	// Number starts with minus '-'
				if($c == '0') {	// After minus can be '0'
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_zero_num;
				}
				else if($c >= '1' && $c <= '9') {// After minus can be '1' - '9'
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_dig;
				}
				else {
					MyFiles::callError(err_number, "Lex Error: After minus '-' must be digit '0'-'9'.\n");
				}
			}
			else if(Scanner::$state == st_zero_num) {// First digit is zero '0'
				if(Scanner::notNumber($c)) {	// Stop read number.
					Scanner::$tmp_string = floor(Scanner::$tmp_string);
					return t_number;
				}
			}
			else if(Scanner::$state == st_num_e) {	//  Check char after 'e'/'E'
				Scanner::JSON_number_e($c);
			}
			else if(Scanner::$state == st_num) {
				if($c >= '1' && $c <= '9') {
					Scanner::$tmp_string .= $c;
					Scanner::$state = st_dig;
				}
				else {
					if(Scanner::notNumber($c)) {	// Stop read number.
						Scanner::$tmp_string = floor(Scanner::$tmp_string);
						return t_number;
					}
				}
			}
			else if(Scanner::$state == st_dig) {
				if($c >= '0' && $c <= '9'){
					Scanner::$tmp_string .= $c;
				}
				else {
					if(Scanner::notNumber($c)) {	// Stop read number.
						Scanner::$tmp_string = floor(Scanner::$tmp_string);
						return t_number;
					}
				}
			}
			else if(Scanner::$state == st_keywords) {
				if(strlen(Scanner::$tmp_string) > 5) {
					MyFiles::callError(err_keywords, "Lex Error: Unknow keywords.\n");
				}
				else {

					Scanner::$tmp_string = Scanner::$tmp_string . $c;
					
					if(Scanner::$tmp_string === 'true') {
						return t_true;
					}
					else if(Scanner::$tmp_string === 'false') {
						return t_false;
					}
					else if(Scanner::$tmp_string === 'null') {
						return t_null;
					}
				}
			}
		}

		return -5;	// My EOF
	}

	/*
		Function returns actual name of returned token
		@return string - name of token
	*/
	public static function getString() {
		return Scanner::$tmp_string;
	}

	/*
		Function check special chars in number.
		If $c == whitespace return true
		@return - false if is not number
	*/
	private static function notNumber($c) {
		if($c == '.') {
			Scanner::JSON_number_point($c);
		}
		else if($c == 'e' || $c == 'E') {
			Scanner::JSON_number_check_e($c);
		}
		else if(Scanner::isWhitespace($c) || $c == ',' || $c == '}' || $c == ']') {
			MyFiles::decPointer();
			return true;
		}
		else {
			echo Scanner::$tmp_string;;
			MyFiles::callError(err_number, "Lex Error: Unexpected character in number.\n");
		}

		return false;
	}

	/*
		Function return true if char is whitespace
		JSON whitespaces: 
	*/
	private static function isWhitespace($c) {
		if($c == ' ' || $c == "\t" || $c == "\n" || $c == "\r")	{	// Space || Hor. Tab || New Line || Carriage return
			return true;
		}
		
		return false;
	}

	/*
		Function check if point was used first time.
	*/
	private static function JSON_number_point($c) {
		if(!Scanner::$num_point) {	// Error: Double point.
			MyFiles::callError(err_number, "Lex Error: Double point in number.\n");
		}
		
		if(!Scanner::$num_e) {	// Error: Used 'e' or 'E' before point '.'
			MyFiles::callError(err_number, "Lex Error: used 'e' or 'E' before point '.'\n");
		}

		Scanner::$tmp_string .= $c;
		Scanner::$num_point = false;
		Scanner::$state = st_dig;
	}

	/*
		Function if e || E is use first time
	*/
	private static function JSON_number_check_e($c) {
		if(!Scanner::$num_e) {	// Error: Double e || E.
			MyFiles::callError(err_number, "Lex Error: Double used 'e' or 'E' in number.\n");
		}

		Scanner::$tmp_string .= $c;
		Scanner::$num_e = false;
		Scanner::$state = st_num_e;
	}

	/*
		Function load string after e || E in number
	*/
	private static function JSON_number_e($c) {
		if($c == '+' || $c == '-' || ($c >= '0' && $c <= '9')) {	//Error: After characters 'e' or 'E' must be '+' or '-' or digits\n
			Scanner::$tmp_string .= $c;
			Scanner::$state = st_dig;
		}
		else {
			echo $c . "\n";
			MyFiles::callError(err_number, "Lex Error: After characters 'e' or 'E' must be '+' or '-' or digits.\n");
		}		
	}

	/*
		Function load char after control char '\'
		If char is 'u', call function for load uniq seq
	*/
	private static function JSON_string($c) {
		Scanner::$state = st_str;

		if ($c == '"' || $c == '\\' || $c == '/') {
			Scanner::$tmp_string .= "$c";
		}
		else if ($c == 'b' || $c == 'f' || $c == "n" || $c == 'r' || $c == 't') {	// Special chars after reverse solidus
			Scanner::$tmp_string .= "\\$c";
		}
		else if( $c == 'u')	{	// Start special sequence
			Scanner::$tmp_string .= "\\$c";
			Scanner::load_uniq();
		}
		else {	// Unexpecting character after control character '\'
			MyFiles::callError(err_string, "Lex Error: Unexpecting character after control character '\'.\n");
		}
	}

	/*
		Function load unique sequence in string.
		Format sequence is \uXXXX.
		Function load XXXX, where X has to be a-f || A-F || 0-9
	*/
	private static function load_uniq() {
		for($i = 0; $i < 4; $i++) {	// After \u must be 4 HEX num
			if(($c = MyFiles::rdChar()) == -1) {	// Error: End of file in unique sequence.
				MyFiles::callError(err_string, "Lex Error: End of file in unique sequence.\n");
			}

			if(($c < '0' || $c > '9') && ($c < 'a' || $c > 'f') && ($c < 'A' || $c > 'F'))	{	// Error: Undefined character in unique sequence.(a-f,A-F,0-9)
				MyFiles::callError(err_string, "Lex Error: Undefined character in unique sequence. Must be (a-f,A-F,0-9).\n");
			}

			Scanner::$string_uniq .= $c;
		}

		Scanner::$tmp_string .= Scanner::$string_uniq;	// Write seq
		Scanner::$string_uniq = "";	// Clean
	}
}
?>