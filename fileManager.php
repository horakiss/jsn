<?php

#JSN:xhorak61
class MyFiles {
	public static $input_file = null;
	private static $input_contents = null;

	private static $output_contents = null;
	public static $output_file = null;

	public static $contents = "";
	private static $pointer = 0;

	/*
		Function opens input file and load content
	*/
    public static function load_input_file($input_file) {
    		$input_file = preg_replace('/^~/', $_SERVER['HOME'],$input_file); // Set tilde '~' to home dir
    		//$input_file = realpath($input_file);
            $handle = @fopen($input_file, 'r');
            self::$input_file = true;
			
            if(!file_exists($input_file)){	// Error: Open file for modify
                    echo "Can't open or doesn't exist input file '$input_file'.\n";
                    exit(2);
            }
            
            if($handle == 0) {	// Error: Open file for modify
                    //echo "Can't open or doesn't exist input file '$input_file'.\n";
                    //exit(2);
            }
            else {
            	fclose($handle);
            	try {
            		self::$input_contents = self::getFileContents($input_file);
            	}
            	catch (Exception $e) {
            		echo "Error: " , $e->getMessage();
            	}
            }
    }

    /*
    	Function opens output file
    */
    public static function load_output_file($output_file)
    {
    		
    		$output_file = preg_replace('/^~/', $_SERVER['HOME'],$output_file); // Set tilde '~' to home dir
    		//$output_file = realpath($output_file);

            $handle = @fopen($output_file, 'w');

            if($handle == 0) {	// Error: Open file for modify
                    //echo "Can't modify output file '$output_file'.\n";
                    //exit(3);
            }
            else {
            		self::$output_file = $output_file;	
            }
            fclose($handle);
    }

    /*
    	Function returns content of file to field.
    */
	public static function getFileContents($path) {
		$path = preg_replace('/^~/', $_SERVER['HOME'],$path); // Set tilde '~' to home dir
		//$path = realpath($path);
	    $content = @file_get_contents($path);
	    if ($content === FALSE) {
	        throw new Exception("Cannot access '$path' to read contents.");
	    }
	    else {
	        return $content;
    	}
	}

	/*
		Function write XML output to file
		If cant write to file, call error
	*/
	public static function putFileContents() {
	    if (@file_put_contents(self::$output_file, XML_generator::getXMLcontents()) === FALSE) {
	        throw new Exception("Cannot access '$path' to read contents.");
	    }
	}

	/*
		Function parses errors and call exit
		$error - Error number, speficified in JSON_scan_lib.php
		$text  - Text of error
		return exit code, speficified in JSON_scan_lib.php
	*/
	public static function callError($error, $text) {

		$STDERR = fopen('php://stderr', 'w+');
		fwrite($STDERR, $text);
		fclose($STDERR);
		exit($error);
	}

	/*
		Function increases pointer of contents field.
	*/
	private static function incPointer() {
		self::$pointer++;
	}

	/*
		Function decreases pointer of contents field.
	*/
	public static function decPointer() {
		self::$pointer--;
	}

	/*
		Function returns pointer of contents field.
	*/
	private static function getPointer() {
		return self::$pointer;
	}

	/*
		Function returns contents of contents field.
	*/
	public static function getContents() {
		self::$contents = self::$input_contents;

	}

	/*
		Function return character from contents field
	*/
	public static function rdChar() {
		if((strlen(self::$contents) - 1) > self::getPointer()) {	// Until not reach end of field
			$temp_char = self::$contents[self::getPointer()];	// Load character
			self::incPointer();	// Increase pointer

			return $temp_char;
		}
		else {
			// End of field
			return my_EOF;
		}
		
	}
}
?>