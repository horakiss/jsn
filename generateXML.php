<?php

#JSN:xhorak61
/*
	Class make XML tags
*/
class XML_generator
{
	public static $array_index = array(); // Stack for values in array in format $i = name/$i+1 = token
	public static $array_size = array();	// Stack save number of items in array
	public static $index_size = 0;	// Stack to count index items
	private static $XML_output = ""; // String of XML tags

	/*
		Generate header if option -n is not set
		Generate tag <root> if option -r= is set
	*/
	public static function header() {
		if(!Settings::$n) {
			self::$XML_output .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		}

		if(Settings::$r != null) {
			Parser::incInd();
			self::$XML_output .= "<" . Settings::$r . ">\n";
		}
	}

	/*
		Generate tag </root> if option -r= is set
	*/
	public static function rootEnd() {
		if(Settings::$r != null) {
			self::$XML_output .= "</" . Settings::$r . ">\n";
		}
	}

	/*
		Generate tag </object>
	*/
	public static function printObjEnd($name) {
		self::$XML_output .= self::writeTabs() . "</" . $name . ">\n";
	}

	/*
		Generate first part of object tag <object
		Check if element name is valid (A-Za-z0-9_)
		Check if element start char is valid (A-Za-z_)
	*/
	public static function printObjName($name) {
		$name = self::elemName($name);
		$name = self::elemfirstChar($name);
		self::$XML_output .= self::writeTabs() . "<" . $name;
	}

	/*
		Generate second part of object tag
		Check if element name is valid (A-Za-z0-9_)
		Check if element start char is valid (A-Za-z_)
	*/
	public static function printObjVal($name, $obj_name, $token) {
		$obj_name = self::elemName($obj_name);
		$obj_name = self::elemfirstChar($obj_name);

		if(Settings::$c && $token = t_string) {	// Convert problematic characters in object value
			$name = self::codeXMLmeta($name);
		}

		if(Settings::$s && $token == t_string || Settings::$i && $token == t_number) {	// Option -s is active and value is string || -i is active and token is number
			self::$XML_output .= ">\n";
			self::$XML_output .= self::writeTabs() . "\t" . $name . "\n";	// Print value as element
			self::$XML_output .= self::writeTabs() . "</" . $obj_name . ">\n";

		}
		else if(Settings::$l && ($token == t_null || $token == t_true || $token == t_false)) { // Option -l is active and value is literar
			self::$XML_output .= ">\n";
			self::$XML_output .= self::writeTabs() . "\t". "<" . $name . "/>\n";	// Print literar as element
			self::$XML_output .= self::writeTabs() . "</" . $obj_name . ">\n";
		}
		else {
			self::$XML_output .= " value=\"" . $name . "\"/>\n";	// Print value as attribut
		}
		
	}

	/*
		Generate end of object or array if it is in array
	*/
	public static function printObjIn() {
		self::$XML_output .= ">\n";
	}

	/*
		Generate tag <array>
	*/
	public static function printArrName() {
		self::$XML_output .= self::writeTabs() . "<" . Settings::$array_name;
		//if(Settings::$index_items && Parser::$result[Parser::$i+2] != 0) {
			//echo " index=\"" . (Parser::$result[Parser::$i+2] + Settings::$start) . "\"";
		//}
		if(Settings::$array_size) {	// If option -a || --array-size is activate
			self::$XML_output .= " size=\"" . current(self::$array_size) . "\"";	// Generate size after <array
			next(self::$array_size);
		}
		
		self::$XML_output .= ">\n";
		Parser::incInd();	// Next items write with bigger indentation
	}

	/*
		Generate tag </array>
	*/
	public static function printArrEnd() {
		array_pop(self::$array_index);	// Pop actual array from stack
		Parser::decInd(); // Set indentation of </array> as <array>
		self::$XML_output .= self::writeTabs() . "</" . Settings::$array_name . ">\n";
	}

	/*
		Generate item in array
	*/
	public static function printItem($name, $token) {

		$tmp_item = self::writeTabs() . "<" . Settings::$item_name;

		if(Settings::$c && $token = t_string) {	// If option -c is active
			$name = self::codeXMLmeta($name);
		}

		// Parser::$result[Parser::$i+2] == 0 mean that item is array in array
		if(Settings::$index_items && Parser::$result[Parser::$i+2] != 0) {	// If option -t || --index-items is set
			$tmp_item .= " index=\"" . (Parser::$result[Parser::$i+2] + Settings::$start) . "\"";
		}

		if(Settings::$s && $token == t_string || Settings::$i && $token == t_number) {	// Same as object val
			self::$XML_output .= $tmp_item . ">\n";
			self::$XML_output .= self::writeTabs() . "\t" . $name . "\n";
			self::$XML_output .= self::writeTabs() . "</" . Settings::$item_name . ">\n";

		}
		else if(Settings::$l && ($token == t_null || $token == t_true || $token == t_false)) { // Same as object val
			self::$XML_output .= $tmp_item . ">\n";
			self::$XML_output .= self::writeTabs() . "\t". "<" . $name . "/>\n";
			self::$XML_output .= self::writeTabs() . "</" . Settings::$item_name . ">\n";
		}
		else {
			self::$XML_output .= $tmp_item . " value=\"" . $name . "\"/>\n";
		}
	}

	/*
		Generate object <item> tag for object in array
	*/
	public static function printItemObjStart() {
		$tmp_item = self::writeTabs() . "<" . Settings::$item_name;
		
		// Parser::$result[Parser::$i+2] == 0 mean that item is object in array
		if(Settings::$index_items && Parser::$result[Parser::$i+2] != 0) { // If option -t || --index-items is set
			$tmp_item .= " index=\"" . (Parser::$result[Parser::$i+2] + Settings::$start) . "\">\n";
		}
		else {
			$tmp_item .= ">\n";
		}
		self::$XML_output .= $tmp_item;
	}

	/*
		Generate tag </item> after end of object in array
	*/
	public static function printItemObjEnd() {
		self::$XML_output .= self::writeTabs() . "</" . Settings::$item_name . ">\n";
	}

	/*
		Function check if first char of element is valid
		@param string - name of element
		@return string - valid first char of element
	*/
	public static function elemfirstChar ($name) {
		$name = preg_replace("[\^]", Settings::$h, $name);
		$name = preg_replace("/^[^A-Ža-ž_:]/", Settings::$h, $name); // If not match with regexp, replace with -h= (default=-)

		if(preg_match("/^[^A-Ža-ž_:]/", $name)) {	// If not allowed, call error 51
			exit(51);	//TODO volat chybu s exit code 51
		}

		return $name;
	}

	/*
		Function check if name of element is valid
		@param string - name of element
		@return string - valid name of element
	*/
	public static function elemName ($name) {
		$name = preg_replace("[\^]", Settings::$h, $name);
		$name = preg_replace("/[^0-9-.A-Ža-ž_:]/", Settings::$h, $name); // If not match with regexp, replace with -h= (default=-)
		//echo $name . "\n";
		if(preg_match("/[^0-9-.A-Ža-ž_:]/", $name)) { // If not allowed, call error 51
			exit(51);	//TODO volat chybu s exit code 51
		}

		return $name;
	}

	/*
		Function check element name choosen by user
		If is not allowed, dont replace and end program
		@param string - name of element
	*/
	public static function checkElement ($name) {
		if(preg_match("/^[^0-9A-Ža-ž_:]/", $name) || preg_match("/[^0-9-.A-Ža-ž_:]/", $name) || $name = preg_match("[\^]", $name)) { // Wrong first char || name
			exit(50);
		}
		//echo "V pohodě";
	}

	/*
		Function code problematic XML characters to XML entities
		@param string - JSON string value
		@return string - coded XML string
	*/
	public static function codeXMLmeta ($name) {
		return  htmlspecialchars($name,ENT_QUOTES);
	}

	/*
		Function make indentation before XML tags
		@return string - tabs \t depends on actual indentation
	*/
	public static function writeTabs() {
		$tmp_string = "";

		for($i = 0; $i < Parser::getInd(); $i++) {
			$tmp_string .= "\t";
		}

		return $tmp_string;
	}

	/*
		Function return XML output
		@return string - strong of XML tags
	*/
	public static function getXMLcontents() {
		return self::$XML_output;
	}
}
?>