<?php

#JSN:xhorak61
/*
	This class load array
*/
class JSON_array {
	
	private $state;	// Actual state
	private $indentation;	// Indentation of array
	private $array_of_val;	// Stack for items of array
	private $arr_index;	// 
	private $my_size;	// Size of array
	private $isEnd;	// End of array

	public function __construct() {
		$this->state = st_array_val;
		$this->array_of_val = array();
		$this->my_size = 0;
		$this->isEnd = false;
		
		$this->arr_index = XML_generator::$index_size;
		array_push(XML_generator::$array_size,0);
		XML_generator::$index_size++;

		$this->indentation = 0;
	}

	/*
		Function represent one array
		@return int - 0 mean end of array
	*/
	public function j_array() {	
		while(($token = Scanner::scannerr()) != -5) {
			if($this->state == st_array_val) {
				if($token == t_object) {
					$call = new JSON_object;
					Parser::pushResult($token, "object", ++$this->indentation);
					$this->my_size++;
					$call->object();
					$this->state = st_array_end;
				}
				else if($token == t_array) {
					$call = new JSON_array;
					Parser::pushResult($token, "array", ++$this->indentation);
					$this->my_size++;
					$call->j_array();
					$this->state = st_array_end;
				}
				else if($token == t_string || $token == t_number || $token == t_true || $token == t_false || $token == t_null) {
					Parser::pushResult($token, Scanner::getString(), ++$this->indentation);
					$this->my_size++;
					array_push($this->array_of_val, Scanner::getString(), $token);
					$this->state = st_array_end;
				}
				else if($token == t_end_array) {
					XML_generator::$array_size[$this->arr_index] = $this->my_size;
					Parser::pushResult($token, "/array", 0);
					return 0;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Expect value in array.\n");
				}
			}
			else if($this->state == st_array_end) {
				if($token == t_end_array) {
					XML_generator::$array_size[$this->arr_index] = $this->my_size;
					Parser::pushResult($token, "/array", 0);
					return 0;
				}
				else if($token == t_comma) {
					$this->state = st_array_val;
					continue;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Expectdd end of array ']' or comma ',' in array.\n");
				}
				
			}
		}
	}

	/*
		Function set indentation of array
	*/
	public function setIndentation($indentation) {
		$this->indentation = $indentation+1;
	}
}
?>