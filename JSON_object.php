<?php

#JSN:xhorak61
/*
	This class load object
*/
class JSON_object {
	private $state;	// State of object
	private $indentation;	// Indentation of object
	private $name;	// Name of object

	public function __construct() {
		$this->state = st_object_name;
		if(Settings::$r != null){
			$this->indentation = 1;
		}
		else {
			$this->indentation = 0;
		}
	}

	/*
		Functions represent one object
		@return int - 0 mean end of object / my_EOF is EOF
	*/
	public function object() {
		while(($token = Scanner::scannerr()) != -5) { // Until EOF
			if($this->state == st_object_name)	{// After '{' expect name of object
				if ($token == t_string) {
					$this->state = st_object_colon;
					Parser::pushResult(t_Ostring, Scanner::getString(), Parser::$actual_depth);
					$this->name = Scanner::getString();
				}
				else if ($token == t_end_object) {
					Parser::pushResult($token, "/object", Parser::$actual_depth);
					return 0;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Expect name of object.\n");
				}
			}
			else if($this->state == st_object_colon) {	// Aftter name of object expect colon ':'
				if($token == t_colon) {
					$this->state = st_object_value;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Expect separator ':' between object name and value.\n");
				}
			}
			else if($this->state == st_object_value) {	// After colon ':' expect value
				if($token == t_object) {
					$call = new JSON_object;
					$call->setIndentation($this->indentation);
					Parser::$actual_depth++;
					Parser::pushResult($token, "object", Parser::$actual_depth);
					$call->object();
					$this->state = st_object_end;
				}
				else if($token == t_array) {
					$call = new JSON_array;
					Parser::pushResult($token, "array", 0);
					$this->state = st_object_end;
					$call->j_array();
				}
				else if($token == t_string || $token == t_number) {
					Parser::pushResult($token, Scanner::getString(), Parser::$actual_depth);
					$this->state = st_object_end;
				}
				else if($token == t_true || $token == t_false || $token == t_null) {
					Parser::pushResult($token, Scanner::getString(), Parser::$actual_depth);
					$this->state = st_object_end;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Expect value of object.\n");
				}
			}
			else if ($this->state == st_object_end) {
				if($token == t_end_object) {
					Parser::pushResult($token, "/object", Parser::$actual_depth);
					return 0;
				}
				else if($token == t_comma) {
					$this->state = st_object_name;
				}
				else {
					MyFiles::callError(err_syn,"Syn Error: Not ended object.\n");
				}	
			}
			else {
				MyFiles::callError(err_syn,"Syn Error: Unknown error in object.\n");
			}
			
		}

		return my_EOF;	// Error file end
	}

	/*
		Function set indentation of object
	*/
	public function setIndentation($indentation) {
		$this->indentation = $indentation+1;
	}
}
?>