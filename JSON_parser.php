<?php

#JSN:xhorak61
require_once('generateXML.php');
require_once('JSON_scanner.php');
require_once('JSON_object.php');
require_once('JSON_array.php');
require_once('settings.php');
/*
	Class control parsing JSON input
	After valid load input, run XML generator
*/
class Parser {

	public static $result = array();	// Save array of tokens for print
	private static $indentation = -1;	// Actual indentation
	public static $actual_depth = 0;	// Actual depth in from main array or object
	public static $i = 0;	// Pointer in result string
	public static $object_names = array();	// Stack for save object names
	public static $context = array(); // Stack for save actual context
	
	/*
		Function start calling scanner and take tokens
		Until dont reach EOF
	*/
	public static function parserr() {

		while(($token = Scanner::scannerr()) != -5) {
			if($token == t_object) {	// Main is object
				
				Parser::pushResult($token, "object",self::$actual_depth);
				$call = new JSON_object;
				$call->object();
				break;
			}
			else if ($token == t_array) {	// Main is array
				Parser::pushResult($token, "array", 0);
				$call = new JSON_array;
				$call->j_array();
				if(self::$result[count(self::$result)-3] != t_end_array) {
					exit(4);
				}
				break;
			}
			else {
				MyFiles::callError(err_syn,"Syn Error: First must be object or field.\n");
			}
		}
		self::printResult();
	}

	/*
		Function push valid token to stack result
	*/
	public static function pushResult($token, $name, $depth) {
		array_push(self::$result, $token, $name, $depth);
	}

	/*
		Function return maximal depth of JSON file
		@return int - maximal depth
	*/
	public static function getMaxDepth($length) {
		$max_dept = 0;

		for($i = 2; $i < $length; $i+=3) {
			if(self::$result[$i] > $max_dept) {
				$max_dept = self::$result[$i];
			}
		}
		return $max_dept;
	}

	/*
		Function start print XML tags from result
	*/
	public static function printResult() {
		$token = self::getTok();
		XML_generator::header();

		if($token == t_object) {	// Main object
			self::new_obj();
		}
		else if($token == t_array) {	// Main array
			Parser::incInd();
			self::new_arr();
		}
		XML_generator::rootEnd();
	}

	/*
		Function that represent start of object in automat
	*/
	public static function new_obj() {
		self::incInd();	// Increase indentation
		array_push(self::$context, t_object);	// Save actual context
		self::in_obj();	// Load name of object
	}

	/*
		Function represent state in object { XXXX: YYYY(,)}
	*/
	public static function in_obj() {
		self::nextTok();// Get next token
		$token = self::getTok();

		if($token == t_Ostring) { // Got object name
			self::obj_name();	
		}
		else if($token == t_end_object || $token == t_end_array) { // End of object or array
			self::decInd(); // Decrase indentation
			array_pop(self::$context); // Close context after name of object or ']'

			if(end(self::$context) == t_array) { // Was in array, rollback pointer a return

				self::$i-=3;
				return 0;
			}
			if(end(self::$context) == t_Ostring) {	// Name of object is element
				XML_generator::printObjEnd(array_pop(self::$object_names));
			}

			array_pop(self::$context); // Close context after '}'

			if(self::getPtr() < count(self::$result) - 3) {	// If isnt EOF, load next pair in object
				self::in_obj();
			}
		}
	}

	/*
		Function save object name and context
	*/
	public static function obj_name() {	
		array_push(self::$object_names, self::getName());	// Save name of object
		array_push(self::$context, t_Ostring);	// Save state

		XML_generator::printObjName(self::getName());	// Print result

		self::obj_val();	// Next will be object value
	}

	/*
		Function load object value
	*/
	public static function obj_val() {
		self::nextTok(); // Get next token
		$token = self::getTok();
		if($token >= t_string && $token <= t_null) {
			XML_generator::printObjVal(self::getName(), array_pop(self::$object_names), $token);
			array_pop(self::$context); //TODO - tady si dát pozor, nějaké podezřelé
			self::in_obj();
		}
		else if($token == t_object) { // Object val is new object
			XML_generator::printObjIn();
			self::new_obj();
		}
		else if($token == t_array) { // Object val is new arry
			XML_generator::printObjIn();

			self::incInd();
			self::new_arr();
			self::decInd();

			self::in_obj();
		}			
		else {
			exit(4);
		}
	}

	/*
		Function create new array
	*/
	public static function new_arr() {
		array_push(self::$context, t_array);	// Save actual context
		XML_generator::printArrName();
		self::in_arr();
	}

	/*
		Function represent state in array
	*/
	public static function in_arr() {
		self::nextTok();	// Get next token
		$token = self::getTok();

		if($token == t_end_array) {
			XML_generator::printArrEnd();
			array_pop(self::$context);
			if(end(self::$context) == t_array) {	// Until context is array
				self::decInd();
				XML_generator::printItemObjEnd();
			}
			
			if(end(self::$context) == t_Ostring) {	// Name of object is element
				self::decInd();
				XML_generator::printObjEnd(array_pop(self::$object_names));
				self::incInd();
				array_pop(self::$context);
			}
		}
		else {	// Val of array
			self::arr();

			if(end(self::$context) == t_array) {	// Until context is array
				if(self::getPtr() < count(self::$result) - 3) { // Until not reach EOF
					self::in_arr();
				}
			}
			
		}

	}
 
	/*
		Function that represent start of array in automat
	*/
	public static function arr() {
		$token = self::getTok();
		if($token >= t_string && $token <= t_null) {	// Value is string || number || null || true || false
			
			XML_generator::printItem(self::getName(), $token);
			
			self::in_arr();
		}
		else if($token == t_array) {	// Value is new array
			XML_generator::printItemObjStart();
			self::incInd();
			self::new_arr();
		}
		else if($token == t_object) {	// Value is new object
			XML_generator::printItemObjStart();
			self::new_obj();
			XML_generator::printItemObjEnd();
			
			self::in_arr();
		}
		else {
			return 0;
		}
	}

	/*
		Function increase actual indentation for print XML tags
	*/
	public static function incInd() {
		self::$indentation++;
	}

	/*
		Function decrease actual indentation for print XML tags
	*/
	public static function decInd() {
		self::$indentation--;
	}

	/*
		Function return actual indentation for print XML tags
		@return int - actual indentation
	*/
	public static function getInd() {
		return self::$indentation;
	}
 
 	/*
		Function return actual pointer for stack in $result
		@return int - actual $result pointer
	*/
	public static function getPtr() {
		return self::$i;
	}

	/*
		Function increase actual pointer for stack in $result
	*/
	public static function nextTok() {
		self::$i+=3;
	}

	/*
		Function return actual token in stack in $result
		@return int - actual $result token
	*/
	public static function getTok() {
		if(self::$i < count(self::$result)) {
			return self::$result[self::$i];
		}
	}

	/*
		Function return name of actual token
		@return string - actual token name
	*/
	public static function getName() {
		return self::$result[self::$i+1];	// +0 - token/ +1 - name / +2 - order
	}
}
?>